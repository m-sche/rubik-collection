# Rubik-collection

Collection of sowtware managing reached times of solving twisty-puzzles. It works with data file `solves.db`, which is a SQLite database in which it stores all times.


## solve.py

Subprogram measuring taking 5 times in a row (a round). At 5th time, short info about current WCA average and best time is printed. You can write `DNF` as a time value.


## grapher.py

This script reads `solves.db` and draws a graph of your performance. Pass a name of the cube as the first argument.


# Copying

Copyright (C) 2013-2020 Martin Scheubrein
Licensed under the terms of the GNU General Public License version 3 (see COPYING)
