#!/usr/bin/env python3

import sqlite3
import argparse
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import seaborn as sns
from datetime import datetime, timedelta
import re

CUBES = ['2x2', '3x3', '4x4', '5x5', '6x6', '3bf', '3oh', '3ft', 'sq1', 'pmx']

def try_parse_date(s, fmt):
	try:
		return datetime.strptime(s, fmt)
	except ValueError:
		return None

def valid_date(s):
	for fmt in ['%Y-%m-%d', '%Y-%m', '%Y']:
		date = try_parse_date(s, fmt)
		if date is not None:
			return date
	raise argparse.ArgumentTypeError(f'Not a valid date: {s}')


parser = argparse.ArgumentParser(description='.')
parser.add_argument('cube', choices=CUBES, help='Cube shape')
parser.add_argument('-f', '--date-from', type=valid_date, default=datetime(1900, 1, 1),
	help='Specify start date as YYYY-MM-DD')
parser.add_argument('-i', '--individual', action='store_true',
	help='Ignore avg5, take individual attempts')
parser.add_argument('-w', '--wrap-date', action='store_true',
	help='Ignore the date axis and show evenly-spaced values')
parser.add_argument('-d', '--distribution', action='store_true',
	help='Show distribution of times')

args = parser.parse_args()

def connect_db():
	conn = sqlite3.connect('solves.db')
	conn.isolation_level = None
	return conn

def load_attempts(conn, args):
	QUERY = 'select date, seconds from attempt join round on (round.id = attempt.round) where cube = ? and date >= ?'

	cur = conn.cursor()
	attempts = []
	for dt, seconds in cur.execute(QUERY, (args.cube, args.date_from)):
		attempts.append({
			'date': datetime.fromisoformat(dt),
			'seconds': seconds
		})
	attempts = pd.DataFrame(attempts).set_index('date')
	attempts = attempts[attempts.seconds.notna()]

	#sns.kdeplot(attempts.loc['2020':].seconds)
	sns.kdeplot(attempts.seconds)
	plt.show()

def load_rounds(conn, args):
	QUERY = 'select date, avg5, best from round where cube = ? and date >= ? order by date'
	if args.individual:
		QUERY = ('select date, seconds as avg5, seconds as best '
		'from attempt join round on (round.id = attempt.round) '
		'where cube = ? and date >= ? order by date')

	cur = conn.cursor()
	rounds = []
	for dt, avg5, best in cur.execute(QUERY, (args.cube, args.date_from)):
		rounds.append({
			'date': datetime.fromisoformat(dt),
			'avg5': avg5, 'best': best
		})
	rounds = pd.DataFrame(rounds).set_index('date')

	resample='D'
	if rounds.index.max() - rounds.index.min() > timedelta(days=365):
		resample = 'W'
	if rounds.index.max() - rounds.index.min() < timedelta(days=30):
		resample = 'H'

	avg5 = rounds.avg5.resample(resample).mean() if not args.wrap_date else rounds.reset_index().avg5
	best = rounds.best.resample(resample).min() if not args.wrap_date else rounds.reset_index().best

	if not args.individual:
		plt.scatter(avg5.index, avg5, s=8)
	plt.scatter(best.index, best, s=8)
	#plt.plot(avg5.index, avg5.rolling(window=(len(avg5)//100+1)).mean())

	tick_freq = 1 if avg5.max() - best.min() < 30 else 5 if avg5.max() - best.min() < 2*60 else 10
	plt.gca().yaxis.set_major_locator(plt.MultipleLocator(tick_freq))

	def mmssformat(time, pos=None):
		if avg5.max() >= 60:
			return f'{int(time // 60)}:{int(time % 60):02}'
		else:
			return int(time)
			
	plt.gca().yaxis.set_major_formatter(mticker.FuncFormatter(mmssformat))


	plt.grid(axis='y')
	plt.show()


if __name__ == '__main__':
	conn = connect_db()

	if args.distribution:
		load_attempts(conn, args)
	else:
		load_rounds(conn, args)

