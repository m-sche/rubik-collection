#!/usr/bin/env python3

import sqlite3
import sys
import re
import os
from datetime import datetime
from statistics import mean
import argparse
import readline
import random
import numpy as np
from functools import partial
import colorama
import subprocess


CUBES = ['2x2', '3x3', '4x4', '5x5', '6x6', '7x7', '3bf', '3oh', '3ft', 'sq1', 'pmx']


class InputTimeFormatError(Exception):
	pass


def connect_db(debug=False):
	conn = sqlite3.connect(':memory:' if debug else 'solves.db')
	conn.isolation_level = None

	cur = conn.cursor()

	cur.execute("""
	create table if not exists round (
		id integer primary key autoincrement,
		cube text not null,
		date text not null,
		avg5 real,
		best real
	)""")

	cur.execute("""
	create table if not exists attempt (
		round integer not null,
		id integer not null,
		seconds real,
		scramble text,
		primary key(round, id)
	)""")

	# Delete void rounds
	cur.execute("""
	with void_rounds as (
		select round.id from round
		left join attempt on (round.id = attempt.round)
		where round.avg5 is null and round.best is null
		group by round.id
		having count(attempt.id) = 0
	)
	delete from round
	where round.id in void_rounds
	""")

	return conn


class Attempt:
	def __init__(self, round_, aid, scramble=None):
		self.id = aid
		self.round = round_
		self.scramble = scramble
		self.seconds = None

	def save(self, seconds):
		self.seconds = seconds

		now = datetime.now().isoformat()
		cur = self.round.conn.cursor()

		cur.execute('insert into attempt (round, id, seconds, scramble) values (?, ?, ?, ?)',
			(self.round.id, self.id, self.seconds, self.scramble))

		self.round.update()


class Round:
	def __init__(self, conn, cube):
		self.conn = conn
		self.cube = cube

		cur = self.conn.cursor()
		now = datetime.now().isoformat()
		cur.execute('insert into round (cube, date) values (?, ?)', (self.cube, now))
		self.id = cur.lastrowid

		self.attempts = []
		self.avg5 = None
		self.best = None
	
	def newattempt(self, **kwargs):
		attempt = Attempt(self, len(self.attempts) + 1, **kwargs)
		self.attempts.append(attempt)
		return attempt
	
	def update(self):
		now = datetime.now().isoformat()
		cur = self.conn.cursor()

		times = [a.seconds for a in self.attempts] + [None] * (5 - len(self.attempts))
		times = sorted(times, key=lambda t: (t is None, t))

		self.avg5 = None if None in times[1:4] else round(mean(times[1:4]), 2)
		self.best = times[0]

		cur.execute('update round set date=?, avg5=?, best=? where id = ?', (now, self.avg5, self.best, self.id))


def parse_seconds(text):
	match = re.match(r'^\s*(dnf|DNF|((\d+):)?(\d{1,2})(\.(\d{1,2}))?)\s*(\[[^\]]+\])?\s*$', text)
	if not match:
		raise InputTimeFormatError()

	scramble = match.group(7)[1:-1] if match.group(7) else None

	if match.group(1).upper() == 'DNF':
		return None, scramble

	minutes = int(match.group(3)) if match.group(3) else 0
	seconds = int(match.group(4)) if match.group(4) else 0
	milisec = int(match.group(6)) if match.group(6) else 0


	return minutes * 60 + seconds + milisec / 100, scramble

class PuzzleBase:
	def gen_scramble(self):
		return None

	def scramble(self, sequence=None):
		if sequence is None:
			sequence = self.gen_scramble()

		for m in sequence:
			self.move(m)
		return sequence

	def move(self, m):
		pass

	def display(self):
		pass


class Cube(PuzzleBase):
	face_colors = [
		colorama.Back.WHITE,							# U
		colorama.Back.GREEN + colorama.Fore.GREEN + colorama.Style.DIM,		# F
		colorama.Back.RED + colorama.Fore.RED + colorama.Style.DIM,		# R
		colorama.Back.YELLOW + colorama.Fore.YELLOW + colorama.Style.DIM,	# D
		colorama.Back.BLUE + colorama.Fore.BLUE + colorama.Style.DIM,		# B
		colorama.Back.MAGENTA + colorama.Fore.YELLOW,				# L
	]

	def __init__(self, size):
		self.size = size
		self.layers = np.array([
			[a, b] if self.size < 4 else [a, a + 'w', b + 'w', b]
			for a, b in [('U', 'D'), ('F', 'B'), ('R', 'L')]
		])

		# Order: U F R D B L
		self.state = np.array([face * np.ones((self.size, self.size), dtype=int) for face in range(6)])

	# The move is bat when it has already been used in current sequence
	# or the puzzle is even and there are two inner-most turns
	def _is_bad_scramble_move(self, x, y, prev):
		return (x, y) in prev or (
			self.size % 2 == 0 and
			y in (self.size//2-1, self.size//2) and
			(x, self.size - 1 - y) in prev
		)

	def gen_scramble(self):
		directions = ['', '\'', '2']
		scramble_size = [0, 0, 14, 25, 40, 60][self.size]

		scramble = []
		prev = tuple()

		for _ in range(scramble_size):
			x = y = None
			while x is None or self._is_bad_scramble_move(x, y, prev):
				x = random.randint(0, self.layers.shape[0] - 1)
				y = random.randint(0, self.layers.shape[1] - 1)

			if len(prev) > 0 and prev[-1][0] == x:
				prev = prev + ((x, y),)
			else:
				prev = ((x, y),)

			#print(self.layers[x, y], [self.layers[p] for p in prev if p is not None])
			scramble.append((x, y))

		dirs = np.random.randint(0, 3, size=scramble_size)
		return [self.layers[m] + directions[d] for m, d in zip(scramble, dirs)]

	def move(self, m):
		# (x y) = what row/col, (a b c d) = what faces
		def permute(a, b, c, d):
			tmp = self.state[a].copy()
			self.state[a] = self.state[d].copy()
			self.state[d] = self.state[c].copy()
			self.state[c] = self.state[b].copy()
			self.state[b] = tmp

		def rot_face(f):
			for i in range(self.size//2):
				left = self.state[f, i:self.size-i, i][::-1].copy()
				top = self.state[f, i, i:self.size-i].copy()
				right = self.state[f, i:self.size-i, -1-i][::-1].copy()
				bottom = self.state[f, -1-i, i:self.size-i].copy()
				self.state[f, i:self.size-i, i] = bottom	# left = bottom
				self.state[f, -1-i, i:self.size-i] = right	# bottom = right
				self.state[f, i:self.size-i, -1-i] = top	# right = top
				self.state[f, i, i:self.size-i] = left		# top = left

		face_num = {'U': 0, 'F': 1, 'R': 2, 'D': 3, 'B': 4, 'L': 5}

		# Tuples of (face, row, col)
		S = slice(None)
		Sr = slice(None, None, -1)
		permutations = {
			'U': ((1,  0,  S), (5,  0,  S), (4,  0,  S), (2,  0,  S)),
			'F': ((0, -1,  S), (2,  S,  0), (3,  0, Sr), (5, Sr, -1)),
			'R': ((0,  S, -1), (4, Sr,  0), (3,  S, -1), (1,  S, -1)),
			'D': ((2, -1,  S), (4, -1,  S), (5, -1,  S), (1, -1,  S)),
			'B': ((5, Sr,  0), (3, -1, Sr), (2,  S, -1), (0,  0,  S)),
			'L': ((1,  S,  0), (3,  S,  0), (4, Sr, -1), (0,  S,  0)),
		}

		def make_perm_inner(inner):
			def _make_perm_inner(perm):
				f, r, c = perm
				if r == 0:
					return (f, r+inner, c)
				elif r == -1:
					return (f, r-inner, c)
				elif c == 0:
					return (f, r, c+inner)
				elif c == -1:
					return (f, r, c-inner)
			return _make_perm_inner

		def turn(f, inner=0):
			permute(*permutations[f])
			rot_face(face_num[f])
			if inner:
				perm = permutations[f]
				perm = [make_perm_inner(p) for p in perm]
				permute(*map(make_perm_inner(inner), permutations[f]))

		inner = 0
		if len(m) > 1 and m[1] == 'w':
			inner = 1

		turn(m[0], inner)
		if m[-1] in ("2", "'"):
			turn(m[0], inner)
		if m[-1] == "'":
			turn(m[0], inner)

	def display(self):
		def cubie(c, face):
			char = ['//', '[]', '[]', r'//', '[]', '[]'][face]
			return self.face_colors[c] + char + colorama.Style.RESET_ALL

		def row(face, r):
			return ''.join(cubie(c, face) for c in self.state[face, r])

		for r in range(self.size):
			print('  ' * self.size + ' ' + row(0, r))
		for r in range(self.size):
			for face in [5, 1, 2, 4]:
				print(row(face, r), end=' ')
			print()
		for r in range(self.size):
			print('  ' * self.size + ' ' + row(3, r))

class Pyraminx(PuzzleBase):
	def __init__(self):
		self.layers = np.array(['U', 'L', 'R', 'B'])

	def gen_scramble(self):
		directions = ['', '\'']
		scramble_size = 8

		scramble = []
		prev = None

		for _ in range(scramble_size):
			m = None
			while m is None or m == prev:
				m = random.randint(0, self.layers.shape[0] - 1)
			prev = m
			scramble.append(m)

		tips = np.array(list(map(str.lower, self.layers)))
		tips = list(tips[np.random.choice([True, False], size=len(tips))])

		tdirs = np.random.randint(0, len(directions), size=len(tips))

		dirs = np.random.randint(0, len(directions), size=scramble_size)
		return [self.layers[m] + directions[d] for m, d in zip(scramble, dirs)] \
			+ [m + directions[d] for m, d in zip(tips, tdirs)]


CUBE_FACTORY = {
	'2x2': partial(Cube, 2),
	'3x3': partial(Cube, 3),
	'3oh': partial(Cube, 3),
	'3bf': partial(Cube, 3),
	'3ft': partial(Cube, 3),
	'4x4': partial(Cube, 4),
	'5x5': partial(Cube, 5),
	'pmx': Pyraminx,
}


def fmt_scramble(scramble):
	maxlen = max(len(m) for m in scramble)
	fmt = f'{{0:{maxlen}}}'

	string = []
	chunk_factor = len(scramble) if len(scramble) < 40 else 20
	for i in range(0, len(scramble), chunk_factor):
		chunk = scramble[i:i+chunk_factor]
		string.append(' '.join(fmt.format(m) for m in chunk))

	return '\n'.join(string)


def epilogue(rd, conn):
	avg = 'DNF' if rd.avg5 is None else rd.avg5
	print(f'Average: {avg}')
	conn.close()

def interactive(cube):
	cube.display()
	move = None

	while True:
		move = input()
		if move == 'q':
			break
		cube.move(move)
		cube.display()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Solve the cube 5 times and record the results.')
	parser.add_argument('cube', choices=CUBES, help='Cube shape')
	parser.add_argument('-g', '--debug', action='store_true', help='Do not save anything')
	parser.add_argument('-i', '--interactive', action='store_true', help='Solve cube interactively')
	parser.add_argument('-s', '--scramble', action='store_true', help='Generate scramble')
	parser.add_argument('-t', '--timer', help='Executable prinitng out the times')

	args = parser.parse_args()

	colorama.init()

	if args.interactive:
		cube = CUBE_FACTORY[args.cube]()
		interactive(cube)
		sys.exit()

	conn = connect_db(debug=args.debug)

	if args.timer:
		timer_process = subprocess.Popen(
			[args.timer],
			cwd=os.path.dirname(args.timer),
			stdout=subprocess.PIPE,
			text=True
		)

	rd = Round(conn, args.cube)
	while len(rd.attempts) < 5:
		scramble = None
		if args.scramble and args.cube in CUBE_FACTORY:
			cube = CUBE_FACTORY[args.cube]()
			scramble = cube.scramble()

			print(fmt_scramble(scramble))
			cube.display()

		while True:
			try:
				prompt = f'Time {len(rd.attempts) + 1}: {colorama.Style.BRIGHT}'
				if args.timer:
					text_input = timer_process.stdout.readline().strip()
					print(f'{prompt}{text_input}')
					if len(text_input) == 0:
						raise KeyboardInterrupt
				else:
					text_input = input(prompt)
				print(colorama.Style.RESET_ALL)

				seconds, user_scramble = parse_seconds(text_input)
				if user_scramble:
					scramble = user_scramble.split() if len(user_scramble) > 0 else None
				break
			except InputTimeFormatError:
				print('Input format not understood')
			except KeyboardInterrupt:
				print(colorama.Style.RESET_ALL)
				epilogue(rd, conn)
				sys.exit(1)

		attempt = rd.newattempt(scramble=(' '.join(scramble) if scramble else None))
		attempt.save(seconds)

	if args.timer:
		timer_process.kill()
	epilogue(rd, conn)
